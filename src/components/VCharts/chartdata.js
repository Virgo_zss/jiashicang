let chartDatas = [
    [
        [{
            title: '饼图1',
            label: {
                name: '用地',
                value: 'gkgn'
            },
            data: {
                columns: ['年龄', '人口数量'],
                rows: [{
                    '年龄': '0',
                    '人口数量': 250
                }, {
                    '年龄': '1',
                    '人口数量': 130
                }, {
                    '年龄': '2',
                    '人口数量': 15
                }, {
                    '年龄': '3',
                    '人口数量': 228
                }]
            },
            settings: {
                itemStyle: function(params) {
                    return params
                }
            },
            tooltip: {
                formatter: (params) => {
                    // 如果有其它年龄段 继续写判断
                    if (params.name == '0') {
                        params.name = '无年龄数据'
                    }
                    params = `${params.name}:${params.value}(${params.percent})%`
                    return params
                }
            },
            legend: {
                formatter: (paramname) => {
                    // 如果有其它年龄段 继续写判断
                    if (paramname == '0') {
                        paramname = '无年龄数据'
                    }
                    return paramname
                }
            },
            type: 'pie'
        }, {
            title: '饼图2',
            label: {
                name: '用途',
                value: 'diew'
            },
            data: {
                columns: ['日期', '访问用户'],
                rows: [{
                    '日期': '1992-03-18',
                    '访问用户': 1609
                }, {
                    '日期': '1996-09-08',
                    '访问用户': 3637
                }, {
                    '日期': '2007-12-29',
                    '访问用户': 3436
                }, {
                    '日期': '1990-08-02',
                    '访问用户': 4465
                }]
            },
            type: 'pie'
                // settings: {
                //     radius: 50,
                //     offsetY: 100
                // }
        }, {
            title: '环形图',
            label: {
                name: '环图',
                value: 'ht'
            },
            data: {
                columns: ['日期', '访问用户'],
                rows: [
                    { '日期': '2018.9.24', '访问用户': 133 },
                    { '日期': '2019.7.29', '访问用户': 35 }
                ]
            },
            type: 'ring'
                // settings: {
                //     radius: 50,
                //     offsetY: 100
                // }
        }],
        [{
                title: '柱图',
                label: {
                    name: '柱图',
                    value: 'hiehh'
                },
                data: {
                    columns: ['日期', '访问用户'],
                    rows: [{
                        '日期': '1986-06-16',
                        '访问用户': 2312
                    }, {
                        '日期': '1971-07-28',
                        '访问用户': 4485
                    }, {
                        '日期': '1994-01-14',
                        '访问用户': 3212
                    }, {
                        '日期': '1992-03-16',
                        '访问用户': 3652
                    }, {
                        '日期': '1993-09-13',
                        '访问用户': 2702
                    }, {
                        '日期': '1986-06-16',
                        '访问用户': 2312
                    }, {
                        '日期': '1971-07-28',
                        '访问用户': 4485
                    }, {
                        '日期': '1994-01-14',
                        '访问用户': 3212
                    }, {
                        '日期': '1992-03-16',
                        '访问用户': 3652
                    }, {
                        '日期': '1993-09-13',
                        '访问用户': 2702
                    }, {
                        '日期': '1986-06-16',
                        '访问用户': 2312
                    }, {
                        '日期': '1971-07-28',
                        '访问用户': 4485
                    }, {
                        '日期': '1994-01-14',
                        '访问用户': 3212
                    }, {
                        '日期': '1992-03-16',
                        '访问用户': 3652
                    }, {
                        '日期': '1993-09-13',
                        '访问用户': 2702
                    }]
                },
                type: 'histogram'
            }, {
                title: '排序条形图',
                label: {
                    name: 'barXXX',
                    value: 'jhfgg'
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 }
                    ]
                },
                type: 'bar',
                settings: {
                    metrics: ['访问用户'],
                    dataOrder: { // 排序
                        label: '访问用户',
                        order: 'desc'
                    }
                }
            },
            {
                title: '折线+柱',
                label: {
                    name: '折线+柱',
                    value: 'jxlh11'
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
                        { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
                        { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
                        { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
                    ]
                },
                type: 'histogram',
                settings: {
                    showLine: ['下单用户']
                }
            },
            {
                title: '堆叠图',
                label: {
                    name: '堆叠',
                    value: 'dkdjf'
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623 }
                    ]
                },
                type: 'histogram',
                settings: {
                    stack: { '用户': ['访问用户', '下单用户'] }
                }
            }
        ]
    ],
    [
        [{
            title: '分组柱图',
            label: {
                name: '分组柱图',
                value: 'aa'
            },
            settings: {
                // axisSite: { right: ['下单率'] },
                // yAxisType: ['KMB', 'percent'],
                // yAxisName: ['数值', '比率'],
                // scale: [true, true],
                // stack: { '用户': ['拆除违建', '人口疏解'] },
                axisSite: { right: ['人口疏解'] },
                yAxisType: ['normal', 'normal'],
                yAxisName: ['拆除违建(单位)', '人口疏解(单位)'],
                min: ['-50', '-50'],
                max: ['50', '50']
            },
            data: {
                columns: ['日期', '拆除违建', '人口疏解'],
                rows: [
                    { '日期': '1/1', '拆除违建': -30.22, '人口疏解': -31.24 },
                    { '日期': '1/2', '拆除违建': -3.38, '人口疏解': -11.79 },
                    { '日期': '1/3', '拆除违建': -2.66, '人口疏解': -10.06 },
                    { '日期': '1/4', '拆除违建': -5.16, '人口疏解': 31.79 }
                ]
            },
            type: 'histogram'
        }, {
            title: '暂无数据',
            label: {
                name: '暂无数据',
                value: 'puew'
            },
            type: 'pie',
            data: {
                columns: ['日期', '访问用户'],
                rows: [
                    // {
                    //     "日期": "2007-09-20",
                    //     "访问用户": 1354
                    // }, {
                    //     "日期": "2000-03-12",
                    //     "访问用户": 2338
                    // }
                ]
            },
            // settings: {
            //     radius: 50,
            //     offsetY: 100
            // }
        }]
    ],
    [
        [
            // {
            //     title: '定制颜色以及最大最小值堆叠柱状图',
            //     label: {
            //         name: '定制颜色以及最大最小值堆叠柱状图',
            //         value: 'danzhexian'
            //     },
            //     type: 'histogram',
            //     data: {
            //         columns: ['日期', '访问用户', '下单用户'],
            //         rows: [
            //             { '日期': '1/1', '访问用户': 536, '下单用户': -11.32 },
            //             { '日期': '1/2', '访问用户': 212, '下单用户': -1.95 },
            //             { '日期': '1/3', '访问用户': 432, '下单用户': -0.75 },
            //             { '日期': '1/4', '访问用户': 311, '下单用户': -0.41 },

            //         ]
            //     },
            //     settings: {
            //         stack: { '用户': ['访问用户', '下单用户'] },
            //         min: ['-600', '-1'],
            //         max: ['600', '1'],
            //         axisSite: { right: ['下单用户'] }
            //     },
            //     extent: {
            //         color: ["#b9e1dd", "#009688"],
            //         label: { show: true, color: '#fff' }
            //     }
            // },
            {
                title: '单折线图',
                label: {
                    name: '单折线图',
                    value: 'danzhexian'
                },
                type: 'line',
                data: {
                    columns: ['日期', '访问用户', '下单用户'],
                    rows: [{
                        '日期': '2014-04-16',
                        '访问用户': 1434,
                        '下单用户': 3706
                    }, {
                        '日期': '2015-11-13',
                        '访问用户': 1387,
                        '下单用户': 4087
                    }, , {
                        '日期': '2016-11-13',
                        '访问用户': 888,
                        '下单用户': 22
                    }]
                },
                extent: {
                    legend: {
                        show: true,
                        // top: -10,
                        // right: 10,
                        // zlevel: 99
                    },
                    color: ["#b9e1dd", "#009688"]
                }
            }
            // , {
            //     title: '词云图',
            //     label: {
            //         name: "积头写变关",
            //         value: "pbuw"
            //     },
            //     type: 'wordcloud',
            //     data: {
            //         columns: ['word', 'count'],
            //         rows: [{
            //             'word': 'visualMap',
            //             'count': 22199
            //         }, {
            //             'word': 'continuous',
            //             'count': 10288
            //         }, {
            //             'word': 'contoller',
            //             'count': 620
            //         }, {
            //             'word': 'gauge',
            //             'count': 12311
            //         }, {
            //             'word': 'detail',
            //             'count': 1206
            //         }]
            //     }
            // }
        ],
        [
            // {
            //       'title': '拆除违建与就业人口疏解',
            //       'dataEmpty': 'true',
            //       'type': 'scatter',
            //       tag: true,
            //       settings: {
            //           xAxisType: 'value',
            //           symbolSize: 10,
            //           dimension: '实施量',
            //           metrics: ['拆违疏解就业人口']
            //               // yAxisName: 'y单位',
            //               // xAxisName: 'x单位',
            //       },
            //       data: {
            //           columns: [
            //               '实施量',
            //               '拆违疏解就业人口'
            //           ],
            //           rows: {
            //               '十八里店乡': [{
            //                   '实施量': 325.58,
            //                   '拆违疏解就业人口': 2.56
            //               }],
            //               '王四营乡': [{
            //                   '实施量': 13.76,
            //                   '拆违疏解就业人口': 0.2
            //               }],
            //               '小红门乡': [{
            //                   '实施量': 19.34,
            //                   '拆违疏解就业人口': 0.17
            //               }],
            //               '平房乡': [{
            //                   '实施量': 32.35,
            //                   '拆违疏解就业人口': 0.17
            //               }]
            //           }
            //       },
            //       tooltip: {
            //           formatter: '{a}'
            //       },
            //       extent: {
            //           series: {
            //               markLine: {
            //                   symbol: 'none',
            //                   lineStyle: {
            //                       normal: {
            //                           type: 'solid'
            //                       }
            //                   },
            //                   data: [
            //                       // {type : 'average', name: '平均值'},
            //                       {
            //                           name: '平均值线',
            //                           yAxis: 2.2 // 数值类型，对应y轴坐标
            //                       },
            //                       { xAxis: 150 }
            //                   ]
            //               }
            //           }
            //       }
            //   },
            // {
            //     title: '散点图',
            //     label: {
            //         name: "ghkl",
            //         value: "11"
            //     },
            //     settings: {
            //         xAxisType: 'value',
            //         symbolSize: 10,
            //         yAxisName: 'y单位',
            //         xAxisName: 'x单位',
            //     },
            //     data: {
            //         columns: ['实施量', '拆违疏解居住人口'],
            //         rows: {
            //             '十八里店乡': [
            //                 { '实施量': 123, '拆违疏解居住人口': 11123 }
            //             ],
            //             '王四营乡': [
            //                 { '实施量': 456, '拆违疏解居住人口': 33954 }
            //             ],
            //             '平房乡': [
            //                 { '实施量': 789, '拆违疏解居住人口': 7531 }
            //             ]
            //         }
            //     },
            //     tooltip: {
            //         formatter: (params) => {
            //             params = params.seriesName + "<br>实施量: " + params.value[0] + "<br>拆违疏解居住人口: " + params.value[1]
            //             return params
            //         }
            //     },
            //     type: "scatter"
            // },
            {
                title: "折线面积图",
                label: {
                    name: "证林折线面积图",
                    value: "ivhq"
                },
                data: {
                    columns: ["日期", "访问用户"],
                    rows: [{
                        "日期": "2009-07-23",
                        "访问用户": 4389
                    }, {
                        "日期": "1977-05-28",
                        "访问用户": 1498
                    }, {
                        "日期": "1980-08-27",
                        "访问用户": 2171
                    }, {
                        "日期": "2008-07-31",
                        "访问用户": 2884
                    }]
                },
                type: "line",
                settings: {
                    // stack: { '用户': ['访问用户'] },
                    area: true
                },
                extent: {
                    xAxis: {
                        axisLabel: {
                            color: '#fff',
                            formatter: '{value}月'
                        },
                        axisLine: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        boundaryGap: false
                    }
                }
            }
        ]
    ]
]

export {
    chartDatas
}