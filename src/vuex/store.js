import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // https://liubin1.thupdi.com:8443/svn/space-plat/branches/zf/onemap-zf20191108/onemap-web
    // 接口相关
    apiConfig: window.appcfg.apiOption,
    // 参数条件相关
    paramsCondition: {
      year: '2020',
      month: '8',
      regioncode: '110000',
      regionname: '北京',
      regionlevel: '1'
      // // 地图相关
      // feature: null
    },
    isReturn: false,
    // mapdata: [],
    // 地图相关
    feature: null,
    mapdata: [],
    // 动态颜色状态
    colorParams: {
      colorindex: 0,
      colorlist: [
        // ['rgb(153,102,204)', 'rgb(255,255,0)', 'rgb(153,114,76)', 'rgb(255,0,0)', 'rgb(0,63,127)', 'rgb(255,0,63)', ''],
        ['#ffeb3b', '#fdd835', '#fbc02d', '#f9a825', '#f57f17', '#fffde7', '#fff9c4', '#fff59d', '#fff176', '#ffee58'],
        ['#e91e63', '#ec407a', '#f06292', '#d81b60', '#c2185b', '#ad1457', '#880e4f', '#fce4ec', '#f8bbd0', '#f48fb1'],
        ['#009688', '#80cbc4', '#4db6ac', '#26a69a', '#00897b', '#00796b', '#00695c', '#004d40', '#b2dfdb', '#e0f2f1'],
        ['#00bcd4', '#80deea', '#4dd0e1', '#26c6da', '#00acc1', '#0097a7', '#00838f', '#006064', '#b2ebf2', '#e0f7fa']
      ]
    },
    // 图斑页tab
    tabvalue: {
      leftval: 'ygyx_ygyx',
      rightval: 'ygyx_ygyx',
      leftyear: '2019',
      rightyear: '2018',
      leftmonth: '12',
      rightmonth: '12',
      activeYearIndexLeft: 0, // 年份的默认状态（没有默认值）
      activeYearIndexRight: 0, // 年份的默认状态（没有默认值）
      isVisibleRight: false, // 是否展示分屏
      isVisibleLeftTime: true, // 左侧时间
      isVisibleRightTime: true // 右侧时间
    },
    // 指标默认
    index: 0,
    // 空间选择（默认北京市）===地图
    regionobj: [
      { regionname: '北京', regioncode: '110000' }
    ],
    leftViewExtent: null,
    leftMapServerUrl: '',
    rightMapServerUrl: '',
    isClick: false,
    // isVisibleRight: false,
    // isVisibleLeftTime: false,
    // isVisibleRightTime: false,
    leftMapView: null,
    rightMapView: null
    // baseMapType: 'dzdt'
  },
  getters: {
    // 请求api基本信息
    curApiUrl: state => `http://${state.apiConfig.apiHost}:${state.apiConfig.apiPort}`,
    paramsCondition: state => state.paramsCondition,
    curyear: state => state.paramsCondition.year,
    curmonth: state => state.paramsCondition.month,
    curRegionCode: state => state.paramsCondition.regioncode,
    curRegionName: state => state.paramsCondition.regionname,
    // 地图相关
    curRegionLevel: state => state.paramsCondition.regionlevel,
    curfeature: state => state.feature,
    curLeftMapServerUrl: state => state.leftMapServerUrl,
    curRightMapServerUrl: state => state.rightMapServerUrl,
    isReturn: state => state.isReturn,
    curmapdata: state => state.mapdata,
    // 地图当前渲染的颜色
    curColorIndex: state => state.colorParams.colorindex,
    curColor: state => state.colorParams.colorlist[state.colorParams.colorindex],
    // 图斑
    isVisibleRightMap: state => state.tabvalue.isVisibleRight,
    isVisibleTimeLeftSpots: state => state.tabvalue.isVisibleLeftTime,
    isVisibleTimeRightSpots: state => state.tabvalue.isVisibleRightTime,
    activeYearIndexLeft: state => state.tabvalue.activeYearIndexLeft,
    activeYearIndexRight: state => state.tabvalue.activeYearIndexRight,
    curLeftTabVal: state => state.tabvalue.leftval,
    curRightTabVal: state => state.tabvalue.rightval,
    curleftyear: state => state.tabvalue.leftyear,
    currightyear: state => state.tabvalue.rightyear,
    curleftmonth: state => state.tabvalue.leftmonth,
    currightmonth: state => state.tabvalue.rightmonth,
    // 指标重置
    curindex: state => state.index,
    curregionobj: state => state.regionobj,
    leftViewExtent: state => state.leftViewExtent,
    isMapClick: state => state.isClick,
    isVisibleRightMap: state => state.tabvalue.isVisibleRight,
    isVisibleTimeLeftSpots: state => state.tabvalue.isVisibleLeftTime,
    isVisibleTimeRightSpots: state => state.tabvalue.isVisibleRightTime,
    leftMapView: state => state.leftMapView,
    rightMapView: state => state.rightMapView
    // curBaseMapType: state => state.baseMapType,
  },
  mutations: {
    // 全局年份
    UPDATEYEAR: (state, payload) => {
      state.paramsCondition.year = payload
    },
    // 全局月份
    UPDATEMONTH: (state, payload) => {
      state.paramsCondition.month = payload
    },
    // 图斑年份状态
    UPDATEACTIVEYEARINDEXLEFT: (state, payload) => {
      state.tabvalue.activeYearIndexLeft = payload
    },
    UPDATEACTIVEYEARINDEXRIGHT: (state, payload) => {
      state.tabvalue.activeYearIndexRight = payload
    },
    UPDATETABVALUE: (state, payload) => {
      state.tabvalue = Object.assign({}, payload)
    },
    // 图斑左年份
    UPDATELEFTYEAR: (state, payload) => {
      state.tabvalue.leftyear = payload
    },
    // 图斑右年份
    UPDATERIGHTYEAR: (state, payload) => {
      state.tabvalue.rightyear = payload
    },
    // 图斑左侧月份
    UPDATELEFTMONTH: (state, payload) => {
      state.tabvalue.leftmonth = payload
    },
    // 图斑右侧月份
    UPDATERIGHTMONTH: (state, payload) => {
      state.tabvalue.rightmonth = payload
    },
    // 地图的code
    UPDATEREGIONCODE: (state, payload) => {
      state.paramsCondition.regioncode = payload
    },
    // 地图的name
    UPDATEREGIONNAME: (state, payload) => {
      state.paramsCondition.regionname = payload
    },
    UPDATEFEATURE: (state, payload) => {
      state.feature = payload
    },
    UPDATERETURNSTATE: (state, payload) => {
      state.isReturn = payload
    },
    UPDATEREGIONLEVEL: (state, payload) => {
      state.paramsCondition.regionlevel = payload
    },
    UPDATECOLORINDEX: (state, payload) => {
      state.colorParams.colorindex = payload
    },
    UPDATEMAPDATA: (state, payload) => {
      // state.mapdata = Object.assign([], payload)
      state.mapdata = payload
    },
    UPDATETABLEFT: (state, payload) => {
      state.tabvalue.leftval = payload
    },
    UPDATETABRIGHT: (state, payload) => {
      state.tabvalue.rightval = payload
    },
    UPDATEINDEX: (state, payload) => {
      state.index = payload
    },
    UPDATEMAPVIEW: (state, payload) => {
      state.leftViewExtent = payload
    },
    UPDATELEFTMAPSERVERURL: (state, payload) => {
      state.leftMapServerUrl = payload
    },
    UPDATERIGHTMAPSERVERURL: (state, payload) => {
      state.rightMapServerUrl = payload
    },
    UPDATEISMAPCLICK: (state, payload) => {
      state.isClick = payload
    },
    // 整体改变属性菜单
    CHANGEPARAMSOBJ: (state, payload) => {
      state.paramsCondition = Object.assign({}, state.paramsCondition, payload)
    },
    CHANGEVISIBLESTATE: (state, payload) => {
      state.tabvalue.isVisibleRight = payload
    },
    CHANGEVISIBLELEFTTIME: (state, payload) => {
      state.tabvalue.isVisibleLeftTime = payload
    },
    CHANGEVISIBLERIGHTTIME: (state, payload) => {
      state.tabvalue.isVisibleRightTime = payload
    },
    UPDATELEFTMAPVIEW: (state, payload) => {
      state.leftMapView = payload
    },
    UPDATERIGHTMAPVIEW: (state, payload) => {
      state.rightMapView = payload
    }
    // UPDATEBASEMAPTYPE: (state, payload) => {
    //   state.baseMapType = payload
    // }
    // UPDATEISSHOWLEFTLAYER: (state, payload) => {
    //     state.isShowLeftLayer = payload
    // },
    // UPDATEISSHOWRIGHTLAYER: (state, payload) => {
    //     state.isShowRightLayer = payload
    //     state.tabvalue.isVisibleRightTime = payload
    // }
  },
  actions: {
    // 更新请求参数
    updateApiParams ({ commit }, payload) {
      commit('UPDATEAPIPARAMS', payload)
    },
    changeyear ({ commit }, payload) {
      commit('UPDATEYEAR', payload)
    },
    // ~~~~~~~~~~ 图斑相关~~~~~~~~~~~~
    changeactiveyearindexleft ({ commit }, payload) {
      commit('UPDATEACTIVEYEARINDEXLEFT', payload)
    },
    changeactiveyearindexright ({ commit }, payload) {
      commit('UPDATEACTIVEYEARINDEXRIGHT', payload)
    },
    changeleftyear ({ commit }, payload) {
      commit('UPDATELEFTYEAR', String(payload))
    },
    changerightyear ({ commit }, payload) {
      commit('UPDATERIGHTYEAR', String(payload))
    },
    changemonth ({ commit }, payload) {
      commit('UPDATEMONTH', String(payload))
    },
    // 图斑月份
    changeleftmonth ({ commit }, payload) {
      commit('UPDATELEFTMONTH', String(payload))
    },
    changerightmonth ({ commit }, payload) {
      commit('UPDATERIGHTMONTH', String(payload))
    },
    changeregioncode ({ commit }, payload) {
      commit('UPDATEREGIONCODE', payload)
    },
    changeregionname ({ commit }, payload) {
      commit('UPDATEREGIONNAME', payload)
    },
    changeregionlevel ({ commit }, payload) {
      commit('UPDATEREGIONLEVEL', payload)
    },
    changefeature ({ commit }, payload) {
      commit('UPDATEFEATURE', payload)
    },
    changereturnstate ({ commit }, payload) {
      commit('UPDATERETURNSTATE', payload)
    },
    changeColorIndex ({ commit }, payload) {
      commit('UPDATECOLORINDEX', payload)
    },
    changeMapData ({ commit }, payload) {
      commit('UPDATEMAPDATA', payload)
    },
    // 图斑所有的参数对象
    changetabvalue ({ commit }, payload) {
      commit('UPDATETABVALUE', payload)
    },
    // 图斑左侧tab值
    changetableft ({ commit }, payload) {
      commit('UPDATETABLEFT', payload)
    },
    // 图斑右侧tab值
    changetabright ({ commit }, payload) {
      commit('UPDATETABRIGHT', payload)
    },
    // 指标默认值
    changeindex ({ commit }, payload) {
      commit('UPDATEINDEX', payload)
    },
    // 图斑左侧url
    changeLeftMapServerUrl ({ commit }, payload) {
      commit('UPDATELEFTMAPSERVERURL', payload)
    },
    // 图斑右侧url
    changeRightMapServerUrl ({ commit }, payload) {
      commit('UPDATERIGHTMAPSERVERURL', payload)
    },
    changeIsMapClick ({ commit }, payload) {
      commit('UPDATEISMAPCLICK', payload)
    },
    // 整体改变属性菜单
    changeParamsObj ({ commit }, payload) {
      commit('CHANGEPARAMSOBJ', payload)
    },
    changeVisibleState ({ commit }, payload) {
      commit('CHANGEVISIBLESTATE', payload)
    },
    // 图斑的时间选择
    changeVisibleLeftTime ({ commit }, payload) {
      commit('CHANGEVISIBLELEFTTIME', payload)
    },
    changeVisibleRightTime ({ commit }, payload) {
      commit('CHANGEVISIBLERIGHTTIME', payload)
    },
    changeLeftMapViewExtent ({ commit }, payload) {
      commit('UPDATEMAPVIEW', payload)
    },
    changeLeftMapView ({ commit }, payload) {
      commit('UPDATELEFTMAPVIEW', payload)
    },
    changeRightMapView ({ commit }, payload) {
      commit('UPDATERIGHTMAPVIEW', payload)
    }
    // changeBaseMapType ({ commit }, payload) {
    //   commit('UPDATEBASEMAPTYPE', payload)
    // },
    // changeIsShowLeftLayer({commit},  payload) {
    //     commit('UPDATEISSHOWLEFTLAYER', payload)
    // },
    // changeIsShowRightLayer({commit},  payload) {
    //     commit('UPDATEISSHOWRIGHTLAYER', payload)
    // }
  }
})
