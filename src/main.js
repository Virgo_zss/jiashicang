import Vue from 'vue'
import App from './Home.vue'
import router from './router/index'
import store from '@/vuex/store'
import '@/assets/base.css'
import '@/assets/response-style.css'
import VCharts from 'v-charts'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/fonts/iconfont.css'
import echarts from 'echarts' // 引入组件

// 滚动条
import GeminiScrollbar from 'vue-gemini-scrollbar'
// 自动播放
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

import axios from 'axios'

import bjGeojson from './utils/bjboundary.js' // 引入北京地图数据

echarts.registerMap('北京边界', bjGeojson)
Vue.use(VueAwesomeSwiper) // 引入echarts
Vue.prototype.$echarts = echarts
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.timeout = 10000
Vue.prototype.$axios = axios

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(VCharts)
Vue.use(GeminiScrollbar)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
