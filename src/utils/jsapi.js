import esriLoader from 'esri-loader'

function configEsriLoader () {
  esriLoader.utils.Promise = Promise
}

export default function preload () {
  return esriLoader.loadScript({
    dojoConfig: window.appcfg.dojoConfig,
    url: window.appcfg.apiRoot
  })
}

export function loadEsriModule (modules) {
  configEsriLoader()
  return esriLoader.loadModules(modules, {
    dojoConfig: window.appcfg.dojoConfig,
    url: window.appcfg.apiRoot
  })
}
