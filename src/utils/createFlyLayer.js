import regionCenters from '@/utils/regionCenters'

import ChartLayer from '@/utils/echartsLayer'
import MapUtils from '@/utils/MapUtils'
function createFlyLayer (view, mapData, odChart) {
  var geoCoordMap = regionCenters[11]
  var allData = mapData// ['series']['RegionData'];//.slice(0, 10);
  let top = 0
  let displayNum = 0
  let fromNum = 1
  let toNum = 0
  if (top == 0) {
    displayNum = allData[0].length
  } else {
    displayNum = top + 1
  }
  let flag = 1
  if (flag == 0) {
    fromNum = 1
    toNum = 0
  } else {
    fromNum = 0
    toNum = 1
  }
  var convertData = function (data) {
    var res = []
    // for (var i = 0; i < data.length; i++) {

    var dataItem = data
    var fromCoord = geoCoordMap[dataItem.fromName]
    var toCoord = geoCoordMap[dataItem.toName]
    if (fromCoord && toCoord) {
      res.push({
        fromName: dataItem.fromName,
        toName: dataItem.toName,
        coords: [fromCoord, toCoord],
        value: dataItem.value
      })
    }
    // }
    return res
  }

  let valueList = []
  for (let i = 0; i < allData.length; i++) {
    valueList.push(allData[i]['value'])
  }
  valueList.sort((a, b) => (b - a))
  let kclass = MapUtils.getJenksBreaks(valueList, 5).sort((a, b) => (a - b))
  let widthList = [0.5, 5, 12, 18, 26]// [1,2,3,5,10]
  let opacityList = [0.1, 0.1, 0.2, 0.4, 0.8]
  let pointSize = [1, 5, 10, 15, 20]
  var color = [ '#009688', '#a6c84c', '#ffa022', '#46bee9']
  var series = []
  var legendData = []
  var labelNames = []
  var planePath = 'path://M.6,1318.313v-89.254l-319.9-221.799l0.073-208.063c0.521-84.662-26.629-121.796-63.961-121.491c-37.332-0.305-64.482,36.829-63.961,121.491l0.073,208.063l-319.9,221.799v89.254l330.343-157.288l12.238,241.308l-134.449,92.931l0.531,42.034l175.125-42.917l175.125,42.917l0.531-42.034l-134.449-92.931l12.238-241.308L1705'
  if (allData.length > 16) {
    allData.forEach(function (item, i) {
      legendData.push(item.fromName)
      series.push(
        {
          coordinateSystem: 'arcgis',
          name: item.fromName,
          type: 'lines',
          zlevel: 2,
          effect: {
            show: false,
            period: 6,
            trailLength: 0,
            // symbol: planePath,
            symbolSize: 3,
            color: '#fff'
          },
          lineStyle: {
            normal: {
              color: color[1],
              // width: 10,
              // opacity: 0.6,
              curveness: 0.2
            }
          },
          data: convertData(item).map((e, i) => {
            let wd = 0
            let opacity = 1
            let color2 = color[1]
            for (let i = 0; i < kclass.length - 1; i++) {
              if (e.value >= kclass[i] && e.value < kclass[i + 1]) {
                wd = widthList[i]
                opacity = opacityList[i]
                break
              }
            }
            if (e.value >= kclass[kclass.length - 1]) {
              wd = widthList[kclass.length - 1]
              opacity = opacityList[kclass.length - 1]
              color2 = '#466218'
            }
            e.lineStyle = {
              width: wd,
              opacity: opacity,
              color: color2
              //   type: cstyle.linesymboltype
            }
            return e
          })
        },
        {
          coordinateSystem: 'arcgis',
          name: item.fromName,
          type: 'effectScatter',
          zlevel: 2,
          rippleEffect: {
            brushType: 'stroke'
          },
          label: {
            normal: {
              show: false,
              position: 'right',
              formatter: function (params) {
                let name = params.name
                return name
                // if(!labelNames.includes(params.name)){
                //   labelNames.push(name)
                //   return name
                // }else{
                //   return ""
                // }
              },
              textStyle: {
                fontSize: 18,
                color: color[1]
              }
            }
          },
          symbolSize: function (val) {
            let value = val[2]
            for (let i = 0; i < kclass.length - 1; i++) {
              if (value >= kclass[i] && value < kclass[i + 1]) {
                return pointSize[i]
              }
            }
            if (value <= kclass[0]) {
              return pointSize[0]
            }
            if (value >= kclass[kclass.length - 1]) {
              return pointSize[kclass.length - 1]
            }
          },
          itemStyle: {
            normal: {
              color: color[1]
            }
          },
          data: [{
            name: item.toName,
            value: geoCoordMap[item.toName].concat([item.value])
          }]
          // data: item.map(function (dataItem) {
          //   return {
          //       name: dataItem.toName,
          //       value: geoCoordMap[dataItem.toName].concat([dataItem.value])
          //   };
          // })
        })
    })
  } else {
    allData.forEach(function (item, i) {
      legendData.push(item.fromName)
      series.push(
        {
          coordinateSystem: 'arcgis',
          name: item.fromName,
          type: 'lines',
          zlevel: 1,
          effect: {
            show: true,
            period: 6,
            trailLength: 0.7,
            color: '#fff',
            symbolSize: 3
          },
          lineStyle: {
            normal: {
              color: color[1],
              width: 0,
              curveness: 0.2
            }
          },
          data: convertData(item)
        },
        {
          coordinateSystem: 'arcgis',
          name: item.fromName,
          type: 'lines',
          zlevel: 2,
          effect: {
            show: true,
            period: 6,
            trailLength: 0,
            // symbol: planePath,
            symbolSize: 3,
            color: '#fff'
          },
          lineStyle: {
            normal: {
              color: color[1],
              // width: 10,
              // opacity: 0.6,
              curveness: 0.2
            }
          },
          data: convertData(item).map((e, i) => {
            let wd = 0
            let opacity = 1
            let color2 = color[1]
            for (let i = 0; i < kclass.length - 1; i++) {
              if (e.value >= kclass[i] && e.value < kclass[i + 1]) {
                wd = widthList[i]
                opacity = opacityList[i]
                break
              }
            }
            if (e.value >= kclass[kclass.length - 1]) {
              wd = widthList[kclass.length - 1]
              opacity = opacityList[kclass.length - 1]
              color2 = '#466218'
            }
            e.lineStyle = {
              width: wd,
              opacity: opacity,
              color: color2
              //   type: cstyle.linesymboltype
            }
            return e
          })
        },
        {
          coordinateSystem: 'arcgis',
          name: item.fromName,
          type: 'effectScatter',
          zlevel: 2,
          rippleEffect: {
            brushType: 'stroke'
          },
          label: {
            normal: {
              show: false,
              position: 'right',
              formatter: function (params) {
                return params.name
              },
              textStyle: {
                fontSize: 18,
                color: color[1]
              }
            }
          },
          symbolSize: function (val) {
            let value = val[2]
            for (let i = 0; i < kclass.length - 1; i++) {
              if (value >= kclass[i] && value < kclass[i + 1]) {
                return pointSize[i]
              }
            }
            if (value <= kclass[0]) {
              return pointSize[0]
            }
            if (value >= kclass[kclass.length - 1]) {
              return pointSize[kclass.length - 1]
            }
          },
          itemStyle: {
            normal: {
              color: color[1]
            }
          },
          data: [{
            name: item.toName,
            value: geoCoordMap[item.toName].concat([item.value])
          }]
        })
    })
  }

  let option = {
    tooltip: {
      trigger: 'item',
      formatter: function (params) {
        if (params.seriesType == 'effectScatter') {
          let value = params.data.value[2]
          if (value > 100) {
            return params.marker + params.data.name + ':' + (value / 10000).toFixed(2) + '万人'
          } else {
            return params.marker + params.data.name + ':' + (value).toFixed(0) + '人'
          }
        } else if (params.seriesType == 'lines') {
          let value = params.data.value
          if (value > 100) {
            return params.data.fromName + '->' + params.data.toName + '<br />' + (params.data.value / 10000).toFixed(2) + '万人'
          } else {
            return params.data.fromName + '->' + params.data.toName + '<br />' + params.data.value.toFixed(0) + '人'
          }
        } else {
          return params.name
        }
      },
      textStyle: {
        color: '#fff',
        fontSize: 16
      }
    },

    legend: {
      // type: 'scroll',
      // orient: 'vertical',
      // top: 'bottom',
      // right: 580,//'right',
      // height: 380,
      // itemWidth: 40,
      // itemHeight: 20,
      data: legendData, // ['上海 Top10','北京 Top10', '广州 Top10'],
      // textStyle: {
      //     color: '#fff',
      //     fontSize: 16
      // },
      // pageIconColor: '#FFC125',
      // pageIconSize: 20,
      // pageTextStyle: {
      //     color: '#fff'
      // },
      // backgroundColor: '#00060a',
      selectedMode: 'multiple',
      show: false
    },
    coordinateSystem: 'arcgis',
    series: series
  }
  if (!odChart) {
    let myChart = new ChartLayer(view, option)
    return myChart.then(function (result) {
      return result
    })
  } else {
    odChart.setChartOption(option)
  }
}

export default createFlyLayer
