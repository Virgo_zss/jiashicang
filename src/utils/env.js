export default {
  getAgsServer () {
    return window.appcfg.agsServer
  },

  getMapServer () {
    return window.appcfg.mapServer
  },

  getInitialExtent () {
    return window.appcfg.initialExtent
  },

  getDefaultVectorMap () {
    return this.getMapServer().vector
  },

  getDefaultRasterMap () {
    return this.getMapServer().raster
  },

  getDefaultBaseMap () {
    return this.getMapServer().baseMap
  },

  getVectorSubLayers () {
    return window.appcfg.vectorSubLayers
  },

  getOtherMapServer () {
    return window.appcfg.otherMapServer
  },
  getGeometryServer() {
    return window.appcfg.geometryService
  },
  getIdentifyLayerIds () {
    return window.appcfg.identifyLayerIds
  },
  getRegionFields(){
    return window.appcfg.regionFields
  },
  getRegionPopup(){
    return window.appcfg.regionPopupTemplate
  },
  getChaiWeiPopup(){
    return window.appcfg.chaiweiPopupTemplate
  }

}
