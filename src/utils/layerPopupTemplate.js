var chaiweiPopupTemplate = {
    '2017': { // 填色时的popup
      title: '图斑号:{id_1}',
      outFields: ['*'],
      content: (
        '<ul>' +
            '<li>图斑位于: {town_1}{village}</li>' +
            '<li>腾退用地面积: {areause}平方米</li>' +
            '<li>违建拆除面积: {areabuil_1}平方米</li>' +
            '</ul>'
      ),
      overwriteActions: true,
      actions: [{
        id: 'drill-btn',
        className: 'esri-icon-overview-arrow-bottom-right',
        title: '查看详情'
      }]
    },
    '2018': {
      title: '图斑号:{id_1}',
      outFields: ['*'],
      content: (
        '<ul>' +
            '<li>图斑位于: {town_1}{village}</li>' +
            '<li>腾退用地面积: {areUse}平方米</li>' +
            '<li>违建拆除面积: {areBld}平方米</li>' +
            '</ul>'
      ),
      overwriteActions: true,
      actions: [{
        id: 'drill-btn',
        className: 'esri-icon-overview-arrow-bottom-right',
        title: '查看详情'
      }]
    },
    '2019': {
      title: '图斑号:{id}',
      outFields: ['*'],
      content: (
        '<ul>' +
            '<li>图斑位于: {town}{village}</li>' +
            '<li>腾退用地面积: {areause}平方米</li>' +
            '<li>违建拆除面积: {areabuild}平方米</li>' +
            '</ul>'
      ),
      overwriteActions: true,
      actions: [{
        id: 'drill-btn',
        className: 'esri-icon-overview-arrow-bottom-right',
        title: '查看详情'
      }]
    },
    '2020': {
      title: '图斑号:{id}',
      outFields: ['*'],
      content: (
        '<ul>' +
            '<li>图斑位于: {town}{village}</li>' +
            '<li>腾退用地面积: {areause}平方米</li>' +
            '<li>违建拆除面积: {areabuild}平方米</li>' +
            '</ul>'
      ),
      overwriteActions: true,
      actions: [{
        id: 'drill-btn',
        className: 'esri-icon-overview-arrow-bottom-right',
        title: '查看详情'
      }]
    }
  }
  
  export {
      chaiweiPopupTemplate
  }