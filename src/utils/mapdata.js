let mapSourceData = {
  'ttyd': [
    {
        "value": 1661.12,
        "regioncode": "110112"
    },
    {
        "value": 1367.37,
        "regioncode": "110115"
    },
    {
        "value": 720.66,
        "regioncode": "110114"
    },
    {
        "value": 717.69,
        "regioncode": "110105"
    },
    {
        "value": 467.18,
        "regioncode": "110113"
    },
    {
        "value": 253.65,
        "regioncode": "110111"
    },
    {
        "value": 248.84,
        "regioncode": "110108"
    },
    {
        "value": 245.93,
        "regioncode": "110106"
    },
    {
        "value": 175.05,
        "regioncode": "110117"
    },
    {
        "value": 150.57,
        "regioncode": "110109"
    },
    {
        "value": 96.8,
        "regioncode": "110116"
    },
    {
        "value": 92.84,
        "regioncode": "110118"
    },
    {
        "value": 64.3,
        "regioncode": "110119"
    },
    {
        "value": 10.78,
        "regioncode": "110101"
    },
    {
        "value": 7.89,
        "regioncode": "110102"
    },
    {
        "value": 1.54,
        "regioncode": "110107"
    }
  ],
  'wjcc': [
    {
        "value": 1262.66,
        "regioncode": "110112"
    },
    {
        "value": 989.96,
        "regioncode": "110115"
    },
    {
        "value": 561.17,
        "regioncode": "110105"
    },
    {
        "value": 480.08,
        "regioncode": "110114"
    },
    {
        "value": 233.65,
        "regioncode": "110113"
    },
    {
        "value": 198.46,
        "regioncode": "110108"
    },
    {
        "value": 193.01,
        "regioncode": "110106"
    },
    {
        "value": 164.94,
        "regioncode": "110111"
    },
    {
        "value": 99.89,
        "regioncode": "110117"
    },
    {
        "value": 82.09,
        "regioncode": "110109"
    },
    {
        "value": 45.98,
        "regioncode": "110116"
    },
    {
        "value": 40.45,
        "regioncode": "110118"
    },
    {
        "value": 26.69,
        "regioncode": "110119"
    },
    {
        "value": 18.56,
        "regioncode": "110101"
    },
    {
        "value": 14.08,
        "regioncode": "110102"
    },
    {
        "value": 0.85,
        "regioncode": "110107"
    }
  ]
}

let regionInfos = [
    {
        "title": "通州区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "1142.9公顷"
            },
            {
                "name": "实施进度",
                "unit": "135%"
            },
            {
                "name": "违建拆除",
                "unit": "923.97万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-3.72万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-1.41万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-1871家"
            }
        ]
    },
    {
        "title": "大兴区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "1139.91公顷"
            },
            {
                "name": "实施进度",
                "unit": "190%"
            },
            {
                "name": "违建拆除",
                "unit": "697.93万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.88万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.29万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-1468家"
            }
        ]
    },
    {
        "title": "昌平区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "840.7公顷"
            },
            {
                "name": "实施进度",
                "unit": "142.7%"
            },
            {
                "name": "违建拆除",
                "unit": "590.22万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-5.68万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-1308家"
            }
        ]
    },
    {
        "title": "朝阳区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "617.83公顷"
            },
            {
                "name": "实施进度",
                "unit": "108.4%"
            },
            {
                "name": "违建拆除",
                "unit": "587.5万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-10.22万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-2.61万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-1767家"
            }
        ]
    },
    {
        "title": "顺义区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "483.32公顷"
            },
            {
                "name": "实施进度",
                "unit": "138.4%"
            },
            {
                "name": "违建拆除",
                "unit": "383.15万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-1.02万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.45万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-423家"
            }
        ]
    },
    {
        "title": "房山区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "389.87公顷"
            },
            {
                "name": "实施进度",
                "unit": "130.0%"
            },
            {
                "name": "违建拆除",
                "unit": "302.41万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-1.07万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.39万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-921家"
            }
        ]
    },
    {
        "title": "丰台区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "245.98公顷"
            },
            {
                "name": "实施进度",
                "unit": "115.5%"
            },
            {
                "name": "违建拆除",
                "unit": "239.26万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-2.66万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-1.07万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-2191家"
            }
        ]
    },
    {
        "title": "海淀区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "213.75公顷"
            },
            {
                "name": "实施进度",
                "unit": "100.3%"
            },
            {
                "name": "违建拆除",
                "unit": "216.92万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-3.38万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-1.39万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-906家"
            }
        ]
    },
    {
        "title": "怀柔区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "146.13公顷"
            },
            {
                "name": "实施进度",
                "unit": "162.4%"
            },
            {
                "name": "违建拆除",
                "unit": "92.1万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.18万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.13万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-3528家"
            }
        ]
    },
    {
        "title": "平谷区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "137.88公顷"
            },
            {
                "name": "实施进度",
                "unit": "137.9%"
            },
            {
                "name": "违建拆除",
                "unit": "105.58万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.54万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.13万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-167家"
            }
        ]
    },
    {
        "title": "门头沟区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "123.52公顷"
            },
            {
                "name": "实施进度",
                "unit": "617.6%"
            },
            {
                "name": "违建拆除",
                "unit": "96.98万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.54万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.19万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-647家"
            }
        ]
    },
    {
        "title": "密云区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "113.9公顷"
            },
            {
                "name": "实施进度",
                "unit": "186.7%"
            },
            {
                "name": "违建拆除",
                "unit": "63.5万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.08万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.04万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-7家"
            }
        ]
    },
    {
        "title": "延庆区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "93.25公顷"
            },
            {
                "name": "实施进度",
                "unit": "233.1%"
            },
            {
                "name": "违建拆除",
                "unit": "49.67万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.29万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.12万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-379家"
            }
        ]
    },
    {
        "title": "东城区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "7.11公顷"
            },
            {
                "name": "实施进度",
                "unit": "355.5%"
            },
            {
                "name": "违建拆除",
                "unit": "13.81万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.61万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.28万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-61家"
            }
        ]
    },
    {
        "title": "西城区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "6.33公顷"
            },
            {
                "name": "实施进度",
                "unit": "316.5%"
            },
            {
                "name": "违建拆除",
                "unit": "12.53万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": "-0.64万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "-0.25万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "-51家"
            }
        ]
    },
    {
        "title": "石景山区",
        "conlists": [
            {
                "name": "用地腾退",
                "unit": "2.67公顷"
            },
            {
                "name": "实施进度",
                "unit": "267%"
            },
            {
                "name": "违建拆除",
                "unit": "1.83万平方米"
            },
            {
                "name": "疏解居住人口",
                "unit": " -0.01万人"
            },
            {
                "name": "疏解就业人口",
                "unit": "0万人"
            },
            {
                "name": "疏解转移企业",
                "unit": "0家"
            }
        ]
    }
]

export {
  mapSourceData,
  regionInfos
}