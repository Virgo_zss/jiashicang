import Vue from 'vue'
import Router from 'vue-router'

// 第1版 专题导向 》空间导航、拆违实施、土地储备
import ThematicGuide from '@/views/ThematicGuide.vue'
import IllegalImplement from '@/views/ThematicGuide/IllegalImplement.vue'
// 第2版 专题导向 》年度违建任务动态监测
import Anualwj from '@/views/ThematicGuide/Anualwj.vue'

// 指标监测》腾退用地、人口疏解、拆除违建、产业提质
import MonitoringIndex from '@/views/MonitoringIndex.vue'
import ttyd from '@/views/MonitoringIndex/ttyd.vue'

// 图斑详情
import SpotsDetail from '@/views/SpotsDetail.vue'

Vue.use(Router)

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/',
    name: 'ThematicGuide',
    component: ThematicGuide,
    redirect: '/thematicguide/anualwj'
  }, {
    path: '/thematicguide',
    name: 'ThematicGuide',
    component: ThematicGuide,
    children: [{
      path: 'illegalimplement',
      name: 'illegalimplement',
      component: IllegalImplement
    }, {
      path: 'anualwj',
      name: 'anualwj',
      component: Anualwj,
      meta: {
        index: 0,
        name: '专题导览'
      }
    }]
  },
  {
    path: '/monitoringindex',
    name: 'MonitoringIndex',
    component: MonitoringIndex
  }, {
    path: '/monitoringindex/ttyd',
    name: 'ttyd',
    component: ttyd,
    meta: {
      index: 1,
      menuTitle: '拆违专题',
      navGuideName: '指标监测',
      name: '用地腾退',
      id: 0,
      year: '2019',
      month: '7',
      path: '/monitoringindex/ttyd'
    }
  }, {
    path: '/monitoringindex/rksj',
    name: 'rksj',
    component: ttyd,
    meta: {
      index: 3,
      menuTitle: '拆违专题',
      navGuideName: '指标监测',
      name: '人口疏解',
      id: 2,
      year: '2018',
      month: '',
      path: '/monitoringindex/rksj'
    }
  }, {
    path: '/monitoringindex/wjcc',
    name: 'wjcc',
    component: ttyd,
    meta: {
      index: 2,
      menuTitle: '拆违专题',
      navGuideName: '指标监测',
      name: '违建拆除',
      id: 1,
      year: '2019',
      month: '7',
      path: '/monitoringindex/wjcc'
    }
  }, {
    path: '/monitoringindex/cytz',
    name: 'cytz',
    component: ttyd,
    meta: {
      index: 4,
      menuTitle: '拆违专题',
      navGuideName: '指标监测',
      name: '产业提质',
      id: 3,
      year: '2018',
      month: '',
      path: '/monitoringindex/cytz'
    }
  }, {
    path: '/spotsdetail',
    name: 'SpotsDetail',
    component: SpotsDetail,
    meta: {
      menuTitle: '拆违专题',
      navGuideName: '图斑详情',
      name: '图斑详情'
    }
  }
  ]
})
