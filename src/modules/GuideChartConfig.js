// 测试数据
let datalists = {
        leftdatas: [{
                id: 0,
                url: '',
                title: '腾退用地',
                time: '2019年12月',
                chartdatas: [{
                        title: '实施量',
                        num: '52.19',
                        unit: '(平方公里)',
                        type: 'bar',
                        chartdata: {
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': 1393 },
                                    { '日期': '1/2', '访问用户': 3530 },
                                    { '日期': '1/3', '访问用户': 2923 },
                                    { '日期': '1/4', '访问用户': 1723 },
                                    { '日期': '1/5', '访问用户': 3792 }
                                ]
                            }
                        }
                    },
                    {
                        title: '实施量',
                        num: '100%',
                        type: 'line',
                        chartdata: {
                            title: '年度实施量',
                            unit: '(平方公里)',
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': 1393 },
                                    { '日期': '1/2', '访问用户': 3530 },
                                    { '日期': '1/3', '访问用户': 2923 },
                                    { '日期': '1/4', '访问用户': 1723 },
                                    { '日期': '1/5', '访问用户': 3792 }
                                ]
                            }
                        },
                        chartSettings: {
                            area: true
                        }
                    }
                ],
            },
            {
                id: 1,
                url: '',
                title: '违建拆除',
                time: '2019年12月',
                chartdatas: [{
                        title: '实施量',
                        num: '52.19',
                        unit: '(平方公里)',
                        type: 'bar',
                        chartdata: {
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': 1393 },
                                    { '日期': '1/2', '访问用户': 3530 },
                                    { '日期': '1/3', '访问用户': 2923 },
                                    { '日期': '1/4', '访问用户': 1723 },
                                    { '日期': '1/5', '访问用户': 3792 }
                                ]
                            }
                        }
                    },
                    {
                        title: '实施量',
                        num: '100%',
                        type: 'line',
                        chartdata: {
                            title: '年度实施量',
                            unit: '(平方公里)',
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': 1393 },
                                    { '日期': '1/2', '访问用户': 3530 },
                                    { '日期': '1/3', '访问用户': 2923 },
                                    { '日期': '1/4', '访问用户': 1723 },
                                    { '日期': '1/5', '访问用户': 3792 }
                                ]
                            }
                        },
                        chartSettings: {
                            area: true
                        }
                    }
                ]
            }
        ],
        rightdatas: [{
                id: 0,
                url: '',
                title: '人口疏解',
                time: '2019年12月',
                chartdatas: [{
                        title: '实施量',
                        num: '52.19',
                        unit: '(平方公里)',
                        type: 'bar',
                        chartdata: {
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': 1393 },
                                    { '日期': '1/2', '访问用户': 3530 },
                                    { '日期': '1/3', '访问用户': 2923 },
                                    { '日期': '1/4', '访问用户': 1723 },
                                    { '日期': '1/5', '访问用户': 3792 }
                                ]
                            }
                        }
                    },
                    {
                        title: '实施量',
                        num: '52.19',
                        unit: '(平方公里)',
                        type: 'bar',
                        chartdata: {
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': 1393 },
                                    { '日期': '1/2', '访问用户': 3530 },
                                    { '日期': '1/3', '访问用户': 2923 },
                                    { '日期': '1/4', '访问用户': 1723 },
                                    { '日期': '1/5', '访问用户': 3792 }
                                ]
                            }
                        }
                    }
                ]
            },
            {
                id: 1,
                url: '',
                title: '产业提质',
                time: '2019年12月',
                chartdatas: [{
                        title: '实施量',
                        num: '52.19',
                        unit: '(平方公里)',
                        type: 'bar',
                        dir: 'F',
                        chartdata: {
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': -1393 },
                                    { '日期': '1/2', '访问用户': -3530 },
                                    { '日期': '1/3', '访问用户': -2923 },
                                    { '日期': '1/4', '访问用户': -1723 },
                                    { '日期': '1/5', '访问用户': -3792 }
                                ]
                            }
                        }
                    },
                    {
                        title: '实施量',
                        unit: '(平方公里)',
                        type: 'ring',
                        chartdata: {
                            data: {
                                columns: ['日期', '访问用户'],
                                rows: [
                                    { '日期': '1/1', '访问用户': 1393 },
                                    { '日期': '1/2', '访问用户': 3530 },
                                    { '日期': '1/3', '访问用户': 2923 },
                                    { '日期': '1/4', '访问用户': 1723 },
                                    { '日期': '1/5', '访问用户': 3792 },
                                    { '日期': '1/6', '访问用户': 4593 }
                                ]
                            }
                        },
                        chartSettings: {
                            roseType: 'radius',
                            radius: 110,
                            label: {
                                show: false
                            }
                        }
                    }
                ]
            }
        ]
    }
    // 图表基本配置属性
let baseConfig = {
    legend: {
        show: false
    },
    grid: {
        left: '10px',
        top: '0px',
        bottom: '10px',
        right: '43px'
    },
    xAxis: {
        axisLabel: {
            show: false
        },
        axisLine: {
            show: false
        },
        splitLine: {
            show: false
        },
        axisTick: {
            show: false
        }
    },
    yAxis: {
        axisLabel: {
            show: false
        },
        axisLine: {
            show: false
        },
        splitLine: {
            show: false
        },
        axisTick: {
            show: false
        }
    },
    series: {
        center: ['50%', '50%'],
        label: {
            show: false
        }
    }
}

// 折线面积
let arealineConfig = {
        ...baseConfig,
        xAxis: {
            axisLabel: {
                color: '#fff'
            },
            axisLine: {
                show: false
            },
            splitLine: {
                show: false
            },
            axisTick: {
                show: false
            }
        },
        series: {
            label: {
                normal: {
                    show: true,
                    color: '#fff'
                }
            }
        }
    }
    // 条形图
let barConfig = {
        ...baseConfig,
        yAxis: {
            axisLabel: {
                color: '#fff'
            },
            axisLine: {
                show: false
            },
            splitLine: {
                show: false
            },
            axisTick: {
                show: false
            }
        }
    }
    // 负数的条形图
let barFConfig = {
    ...baseConfig,
    yAxis: {
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            show: false
        },
        splitLine: {
            show: false
        },
        axisTick: {
            show: false
        }
    },
    xAxis: {
        inverse: true,
        splitLine: {
            show: false
        },
        axisLabel: {
            show: false
        },
    },
    series: {
        barWidth: '60%',
    }
}
export { datalists, baseConfig, arealineConfig, barConfig, barFConfig }