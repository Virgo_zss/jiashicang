import echarts from 'echarts'
let dataliststest = [{
        id: 1,
        url: '/monitoringindex/ttyd',
        title: '用地腾退',
        time: '北京市全市，2019年7月',
        children: [{
                txt: [
                    { name: '任务量', unit: '(平方公里)', num: '999', chartname: '' },
                    { name: '实施进度', unit: '', num: '', chartname: '' }
                ],
                imptitle: '实施进度',
                type: 'bar',
                value: 80,
                data: {
                    columns: ['日期', '访问用户'],
                    rows: [
                        { '日期': '1/1', '访问用户': 80 }
                    ]
                }
            },
            {
                txt: [
                    { name: '实施量', unit: '(平方公里)', num: '999', chartname: '' }
                ],
                type: 'arealine',
                settings: {
                    stack: { '用户': ['访问用户'] },
                    area: true
                },
                data: {
                    columns: ['日期', '访问用户'],
                    rows: [
                        { '日期': '1/1', '访问用户': 2 },
                        { '日期': '1/2', '访问用户': 5 },
                        { '日期': '1/3', '访问用户': 3 }
                    ]
                }
            },
            {
                txt: [
                    { name: '实施量排名', unit: '(平方公里)', num: '', chartname: '' }
                ],
                type: 'bar',
                gridtag: true, // true 代表1 false 代表2
                settings: {
                    metrics: ['访问用户'],
                    dataOrder: {
                        label: '访问用户',
                        order: 'desc'
                    }
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
                        { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
                        { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
                        { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
                    ]
                }
            }
        ]
    },
    {
        id: 2,
        url: '/monitoringindex/rksj',
        title: '人口疏解',
        time: '北京市五环内，2018年',
        children: [{
                txt: [
                    { name: '疏解转移居住人口', unit: '(万人)', num: '999', chartname: '' },
                    { name: '疏解转移就业人口', unit: '(万人)', num: '999', chartname: '' }
                ]
            },
            {
                txt: [
                    { name: '居住人口排名', unit: '(万人)', num: '', chartname: '' }
                ],
                type: 'bar',
                settings: {
                    metrics: ['访问用户'],
                    dataOrder: {
                        label: '访问用户',
                        order: 'desc'
                    }
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
                        { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
                        { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
                        { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
                    ]
                }
            },
            {
                txt: [
                    { name: '就业人口排名', unit: '(万人)', num: '', chartname: '' }
                ],
                type: 'bar',
                settings: {
                    metrics: ['访问用户'],
                    dataOrder: {
                        label: '访问用户',
                        order: 'desc'
                    }
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
                        { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
                        { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
                        { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
                    ]
                }
            }
        ]
    },
    {
        id: 3,
        url: '/monitoringindex/wjcc',
        title: '拆除违建',
        time: '北京市全市，2019年7月',
        children: [{
                txt: [
                    { name: '任务量', unit: '(平方公里)', num: '999', chartname: '实施进度' },
                    { name: '实施进度', unit: '', num: '', chartname: '' }
                ],
                type: 'bar',
                value: 70,
                data: {
                    columns: ['日期', '访问用户'],
                    rows: [
                        { '日期': '1/1', '访问用户': 80 }
                    ]
                }
            },
            {
                txt: [
                    { name: '实施量', unit: '(平方公里)', num: '999', chartname: '' }
                ],
                type: 'arealine',
                settings: {
                    stack: { '用户': ['访问用户'] },
                    area: true
                },
                data: {
                    columns: ['日期', '访问用户'],
                    rows: [
                        { '日期': '1/1', '访问用户': 2 },
                        { '日期': '1/2', '访问用户': 5 },
                        { '日期': '1/3', '访问用户': 3 }
                    ]
                }
            },
            {
                txt: [
                    { name: '实施量排名', unit: '(平方公里)', num: '', chartname: '' }
                ],
                type: 'bar',
                settings: {
                    metrics: ['访问用户'],
                    dataOrder: {
                        label: '访问用户',
                        order: 'desc'
                    }
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
                        { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
                        { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
                        { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
                    ]
                }
            }
        ]
    },
    {
        id: 4,
        url: '/monitoringindex/cytz',
        title: '产业提质',
        time: '北京市五环内，2018年',
        children: [{
                txt: [
                    { name: '清退企业数量', unit: '(家)', num: '999', chartname: '' }
                ]
            },
            {
                txt: [
                    { name: '清退企业数量排名', unit: '(家)', num: '', chartname: '' }
                ],
                type: 'bar',
                settings: {
                    metrics: ['访问用户'],
                    dataOrder: {
                        label: '访问用户',
                        order: 'desc'
                    }
                },
                data: {
                    columns: ['日期', '访问用户', '下单用户', '下单率'],
                    rows: [
                        { '日期': '1/1', '访问用户': 1393, '下单用户': 1093, '下单率': 0.32 },
                        { '日期': '1/2', '访问用户': 3530, '下单用户': 3230, '下单率': 0.26 },
                        { '日期': '1/3', '访问用户': 2923, '下单用户': 2623, '下单率': 0.76 },
                        { '日期': '1/4', '访问用户': 1723, '下单用户': 1423, '下单率': 0.49 },
                        { '日期': '1/5', '访问用户': 3792, '下单用户': 3492, '下单率': 0.323 },
                        { '日期': '1/6', '访问用户': 4593, '下单用户': 4293, '下单率': 0.78 }
                    ]
                }
            },
            {
                txt: [
                    { name: '清退企业所属产业结构', unit: '(家)', num: '', chartname: '' }
                ],
                type: 'pie',
                tag: false,
                data: {
                    columns: ['日期', '访问用户'],
                    rows: [
                        { '日期': '1/1', '访问用户': 223 },
                        { '日期': '1/2', '访问用户': 180 },
                        { '日期': '1/3', '访问用户': 330 }
                    ]
                }
            }
        ]
    }
]

// 图表的基本配置属性
let baseConfigs = {
    grid: {
        bottom: 8,
        top: 8,
        right: 20,
        left: 10
    },
    legend: {
        textStyle: {
            color: '#fff'
        },
        show: false,
        top: 0
            // 图例 滚动属性
            // type: 'scroll',
            // 按钮内容设置
            // pageIconColor: '#a9adb7',
            // pageIconInactiveColor: '#a9adb7',
            // pageTextStyle: {
            //   color: '#a9adb7'
            // },
            // 按钮与图例距离
            // pageButtonGap: 15
    },
    xAxis: {
        axisLabel: {
            color: '#fff',
            interval: 0
        },
        axisLine: {
            show: false
        },
        splitLine: {
            show: false
        },
        axisTick: {
            show: false
        }
    },
    yAxis: {
        axisLabel: {
            show: false
        },
        axisLine: {
            show: false
        },
        splitLine: {
            show: false
        },
        axisTick: {
            show: false
        }
    },
    series: {
        barWidth: '30%',
        label: {
            show: false
        }
    }
}

// 堆叠,柱形有label
let stackYLabel = {
        ...baseConfigs,
        series: {
            barWidth: 20,
            label: {
                show: true,
                position: 'top',
                color: '#fff'
            }
        }
    }
    // 条形图有label
let barYLabel = {
    ...baseConfigs,
    grid: {
        bottom: '1%',
        top: '18%',
        right: '3%',
        left: '3%'
    },
    xAxis: {
        show: false
            // inverse: true
    },
    yAxis: {
        axisLabel: {
            show: true,
            color: '#fff'
        }
    },
    series: {
        barWidth: '55%',
        label: {
            show: true,
            position: 'right',
            color: '#fff',
            formatter: '{c}'
        }
    }
}

let barImpProgressLabel = {
    ...baseConfigs,
    grid: {
        bottom: '10%',
        top: '1%',
        right: '20%',
        left: '20%'
    },
    xAxis: {
        show: false
    },
    series: {
        barWidth: '60%',
        label: {
            show: true,
            position: 'right',
            color: '#fff',
            formatter: '{c}%'
        }
    }
}

// 饼图有图例
let pielegendYNL = {
    legend: {
        orient: 'vertical',
        show: true,
        x: '70%',
        y: 'center',
        textStyle: {
            color: '#fff'
        }
    },
    series: {
        radius: '75%',
        center: ['50%', '50%'],
        roseType: 'radius',
        label: {
            normal: {
                position: 'inner',
                formatter: '{c}',
                textStyle: {
                    color: '#fff',
                    fontWeight: 'bold',
                    fontSize: 14
                },
                show: false
            }
        }
    }
}

// 饼图无图例（还可设置标签）
let pielegendNYL = {
        legend: {
            show: false,
            orient: 'vertical',
            x: '70%',
            y: 'center',
            textStyle: {
                color: '#fff'
            }
        },
        series: {
            radius: '80%',
            center: ['40%', '50%'],
            roseType: 'radius',
            label: {
                normal: {
                    position: 'inner',
                    formatter: '{c}',
                    textStyle: {
                        color: '#fff',
                        fontWeight: 'bold',
                        fontSize: 14
                    },
                    show: false
                }
            }
        }
    }
    // 玫瑰图
let ring = {
    title: {
        text: '17.7',
        subtext: '进度50%...',
        x: '30%',
        y: 'center',
        textStyle: {
            fontWeight: 'normal',
            color: '#ffc612',
            fontSize: '30'
        }
    },
    legend: {
        show: false,
        orient: 'vertical',
        icon: 'circle',
        x: '70%',
        y: 'center',
        textStyle: {
            color: '#fff'
        }
    },
    series: {
        center: ['40%', '50%'],
        radius: [50, 60],
        label: {
            show: false
        },
        labelLine: {
            show: false
        }
    }
}

let chartColors = [
    ['#ffeb3b', '#fdd835', '#fbc02d', '#f9a825', '#f57f17', '#fffde7', '#fff9c4', '#fff59d', '#fff176', '#ffee58'],
    ['#009688', '#80cbc4', '#4db6ac', '#26a69a', '#00897b', '#00796b', '#00695c', '#004d40', '#b2dfdb', '#e0f2f1'],
    ['#e91e63', '#ec407a', '#f06292', '#d81b60', '#c2185b', '#ad1457', '#880e4f', '#fce4ec', '#f8bbd0', '#f48fb1'],
    ['#00bcd4', '#80deea', '#4dd0e1', '#26c6da', '#00acc1', '#0097a7', '#00838f', '#006064', '#b2ebf2', '#e0f7fa']
]

// 面积折线
let arealineConfig = {
    ...baseConfigs,
    grid: {
        top: 12,
        bottom: 22,
        right: 10,
        left: 10
    },
    legend: {
        show: false
    }
}

// 导览页 条形图一样长度
let gridbarguide1 = {
    ...baseConfigs,
    ...barYLabel,
    tooltip: {
        show: false
    },
    grid: {
        bottom: 0,
        top: '0%',
        right: '20%',
        left: '3%'
    },
    xAxis: {
        axisLabel: {
            show: false,
            color: '#fff',
            interval: 0
        },
        axisLine: {
            show: false
        },
        splitLine: {
            show: false
        },
        axisTick: {
            show: false
        },
        inverse: true
    }
}

let gridbarguide2 = {
    ...baseConfigs,
    ...barYLabel,
    tooltip: {
        show: false
    },
    grid: {
        bottom: 0,
        top: '0%',
        right: '20%',
        left: '3%'
    }
}

let barImpProgress = {
    ...baseConfigs,
    xAxis: {
        axisLabel: {
            show: false,
            color: '#fff',
            interval: 0
        },
        axisLine: {
            show: false
        },
        splitLine: {
            show: false
        },
        axisTick: {
            show: false
        }
    }
}

export { dataliststest, chartColors, baseConfigs, stackYLabel, barYLabel, pielegendYNL, pielegendNYL, ring, arealineConfig, gridbarguide1, barImpProgressLabel, gridbarguide2 }