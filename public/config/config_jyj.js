(function (window) {
  let config = {
    // 请求api基本信息
    apiOption: {
      apiHost: '222.65.100.206',
      apiPort: '5005'
    },
    // spotImg2018: 'http://192.164.129.18:8181/xzimg20181220/',
    spotImg2019_1: 'http://222.65.100.202:8021/2019before/',
    spotImg2019_2: 'http://222.65.100.202:8021/2019after/',
    spotImg2020_1: 'http://222.65.100.202:8021/2020before/',
    spotImg2020_2: 'http://222.65.100.202:8021/2020after/',

    apiRoot: 'http://222.65.100.206:8091/arcgis_js_v411_api/arcgis_js_api/library/4.11/init.js',

    agsFonts: 'http://222.65.100.206:8091/examples/jsp/fonts',

    fbServer: 'http://222.65.100.206:8091',

    urlPrefix: [
      'http://222.65.100.205:6080/'
    ],
    proxyUrl: 'http://222.65.100.206:8091/Java/proxy.jsp',

    dojoConfig: {
      async: true,
      deps: ['@dojo/framework/shim/main'],
      has: {
        'esri-promise-compatibility': 1, // Use native Promises by default
        'esri-featurelayer-webgl': 1 // Enable FeatureLayer WebGL capabilities
      }
    },

    agsServer: 'http://222.65.100.205:6080/arcgis/',

    mapServer: {
      vector: 'http://222.65.100.205:6080/arcgis/rest/services/XZQHJ2016/MapServer',
      raster: 'http://222.65.100.205:6080/arcgis/rest/services/DOM201912/MapServer', // 1
      baseMap: {
        'dzdt': 'http://222.65.100.205:6080/arcgis/rest/services/DZDT20201/MapServer',
        'ygyx': 'http://222.65.100.205:6080/arcgis/rest/services/DOM201912/MapServer'
      },
      xzUrl: 'http://222.65.100.205:6080/arcgis/rest/services/XZQHJ2016/MapServer/5', // 乡镇url
      qxUrl: 'http://222.65.100.205:6080/arcgis/rest/services/XZQHJ2016/MapServer/4', // 区县url
      cunUrl: 'http://222.65.100.205:6080/arcgis/rest/services/XZQHJ2016/MapServer/0'
    },
    vectorSubLayers: [{
      id: 4,
      title: '北京村界',
      visible: false,
      source: {
        type: "data-layer",
        dataSource: {
          type: "join-table",
          leftTableSource: {
            type: "map-layer",
            mapLayerId: 0
          },
          rightTableSource: {
            type: "data-layer",
            dataSource: {
              type: "table",
              workspaceId: "BJRegionWorkspaceID",
              dataSourceName: "cunjie"
            }
          },
          leftTableKey: "XZQDM",
          rightTableKey: "XZQDM1",
          joinType: "left-outer-join"
        }
      },
      renderer: {
        type: 'simple',
        symbol: {
          type: 'simple-fill',
          color: [0, 0, 0, 0],
          outline: {
            width: 2,
            color: [255, 255, 255, 1]
          }
        }
      },
      labelingInfo: [{
        labelExpression: '[cunjie.XZQMC1]',
        labelPlacement: 'always-horizontal',
        symbol: {
          type: 'text',
          color: [255, 255, 255, 1],
          haloColor: 'black',
          haloSize: 2,
          font: {
            family: '黑体',
            weight: 'bolder',
            size: 12
          }
        },
        minScale: 2000000,
        maxScale: 500
      }],
      popupEnabled: false
    }, {
      id: 3,
      title: '北京边界',
      visible: false,
      source: {
        mapLayerId: 3
      },
      renderer: {
        type: 'simple',
        symbol: {
          type: 'simple-fill',
          color: [0, 0, 0, 0],
          outline: {
            width: 2,
            color: [190, 190, 190, 1]// '#0692a4',
          }
        }
      }
    },
    {
      id: 2,
      title: '北京乡镇界(底图)',
      visible: false,
      source: {
        mapLayerId: 5
      },
      renderer: {
        type: 'simple',
        symbol: {
          type: 'simple-fill',
          color: [0, 0, 0, 0],
          outline: {
            width: 1,
            color: [190, 190, 190, 1]
          }
        }
      },
      labelingInfo: [{
        labelExpression: '[标准名称]',
        labelPlacement: 'always-horizontal',
        symbol: {
          type: 'text',
          color: [255, 255, 255, 0.5],
          font: {
            family: '黑体',
            // weight: "bold",
            size: 14
          }
        },
        minScale: 2000000,
        maxScale: 500
      }],
      popupEnabled: false
    },
    {
      id: 1,
      title: '北京区县界',
      visible: true,
      labelsVisible: false,
      source: {
        mapLayerId: 4
      },
      renderer: {
        type: 'simple',
        symbol: {
          type: 'simple-fill',
          color: [0, 0, 0, 0],
          outline: {
            width: 1,
            color: [255, 255, 255, 1]// '#0692a4',
          }
        }
      },
      labelingInfo: [{
        labelExpression: '[标准名称]',
        labelPlacement: 'always-horizontal',
        symbol: {
          type: 'text',
          color: [255, 255, 255, 0.5],
          font: {
            family: '黑体',
            // weight: "bold",
            size: 14
          }
        },
        minScale: 2000000,
        maxScale: 500
      }]
    }, {
      id: 0,
      title: '北京乡镇界',
      visible: false,
      source: {
        mapLayerId: 5
      },
      renderer: {
        type: 'simple',
        symbol: {
          type: 'simple-fill',
          color: [0, 0, 0, 0],
          outline: {
            width: 1,
            color: [190, 190, 190, 1]// '#0692a4',
          }
        }
      },
      labelsVisible: false,
      labelingInfo: [{
        labelExpression: '[标准名称]',
        labelPlacement: 'always-horizontal',
        symbol: {
          type: 'text',
          color: [255, 255, 255, 0.5],
          font: {
            family: '黑体',
            // weight: "bold",
            size: 14
          }
        },
        minScale: 2000000,
        maxScale: 500
      }]
    }
    ],
    regionFields: {
      codeField: '政区代码',
      nameField: '标准名称',
      xzCodeField: '乡镇代码',
      qxCodeField: '所属区县'
    },
    // 识别的行政区划图层Id
    identifyLayerIds: [4, 5],
    initialExtent: {
      center: [560732.86379183, 358939.1640053821],
      center2: [658688.8501325934,359509.2652903868],
      wkt: 'PROJCS["用户自定义投影系",GEOGCS["GCS_Krasovsky_1940",DATUM["D_Krasovsky_1940",SPHEROID["Krasovsky_1940",6378245.0,298.3]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Transverse_Mercator"],PARAMETER["False_Easting",500000.0],PARAMETER["False_Northing",300000.0],PARAMETER["Central_Meridian",116.350253],PARAMETER["Scale_Factor",1.0],PARAMETER["Latitude_Of_Origin",39.865767],UNIT["Meter",1.0]]',
      scale: 1000000,
      maxScale: 500,
      minScale: 2000000,
      extent: {
        xmax: 694571.2548989338,
        xmin: 405318.5103903819,
        ymax: 469313.70434796315,
        ymin: 252879.83258282294
      },
      extent2:{
        xmax: 1157958.5986720906,
        xmin: 159419.10159309616,
        ymax: 469311.56822832604,
        ymin: 249706.96235244756,
      }
    },

    otherMapServer: {
      chaiwei: {
        '2019': 'http://222.65.100.205:6080/arcgis/rest/services/CWTB2019/MapServer',
        '2020': 'http://222.65.100.205:6080/arcgis/rest/services/CWTB2020/MapServer',
      }
    },
    // 拆违图斑弹窗配置
    chaiweiPopupTemplate: {
      '2017': { // 填色时的popup
        title: '图斑号:{id_1}',
        outFields: ['*'],
        content: (
          '<ul>' +
              '<li>图斑位于: {town_1}{village}</li>' +
              '<li>腾退用地面积: {areause}平方米</li>' +
              '<li>违建拆除面积: {areabuil_1}平方米</li>' +
              '</ul>'
        ),
        overwriteActions: true,
        actions: [{
          id: 'drill-btn',
          className: 'esri-icon-overview-arrow-bottom-right',
          title: '查看详情'
        }]
      },
      '2018': {
        title: '图斑号:{id_1}',
        outFields: ['*'],
        content: (
          '<ul>' +
              '<li>图斑位于: {town_1}{village}</li>' +
              '<li>腾退用地面积: {areUse}平方米</li>' +
              '<li>违建拆除面积: {areBld}平方米</li>' +
              '</ul>'
        ),
        overwriteActions: true,
        actions: [{
          id: 'drill-btn',
          className: 'esri-icon-overview-arrow-bottom-right',
          title: '查看详情'
        }]
      },
      '2019': {
        title: '图斑号:{id}',
        outFields: ['*'],
        content: (
          '<ul>' +
              '<li>图斑位于: {town}{village}</li>' +
              '<li>腾退用地面积: {areause}平方米</li>' +
              '<li>违建拆除面积: {areabuild}平方米</li>' +
              '</ul>'
        ),
        overwriteActions: true,
        actions: [{
          id: 'drill-btn',
          className: 'esri-icon-overview-arrow-bottom-right',
          title: '查看详情'
        }]
      },
      '2020': {
        title: '图斑号:{id}',
        outFields: ['*'],
        content: (
          '<ul>' +
              '<li>图斑位于: {town}{village}</li>' +
              '<li>腾退用地面积: {areause}平方米</li>' +
              '<li>违建拆除面积: {areabuild}平方米</li>' +
              '</ul>'
        ),
        overwriteActions: true,
        actions: [{
          id: 'drill-btn',
          className: 'esri-icon-overview-arrow-bottom-right',
          title: '查看详情'
        }]
      }
    },
    // 行政区划弹窗配置
    regionPopupTemplate: {
      title: '<h4 style="color:yellow">{标准名称}</h4>',
      content: (
        '<ul>' +
              '<li>日期: {year}年{month}</li>' +
              '<li>累计腾退用地面积: {area}公顷</li>' +
              '<li>累计违建拆除面积: {renderval}万平方米</li>' +
              '</ul>'
      ),
      overwriteActions: true,
      actions: [{
        id: 'drill-btn',
        className: 'esri-icon-overview-arrow-bottom-right',
        title: '查看详情'
      }],
      outFields: ['*']
    },

  }

  window.appcfg = config
  // console.log('进来 window.appcfg')
})(window)
